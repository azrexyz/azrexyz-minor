/*

    <div id="home-video-container" class="home-video-container loading">
      <div class="home-video-player">
        <video muted preload="auto">
          <source src="{$baseUri}/Front/template.pavla-andrle.design/video/hp-andle-design-video.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>
        <div class="play-overlay">
          <a class="play-btn"></a>
        </div>
      </div>
      <div class="video-control-pannel">
        <div class="video-progress">
          <div class="video-progress-in"></div>
        </div>
        <div class="video-controls">
          <a class="play-pause" href="javascript:void(0);"></a>
        </div>
      </div>
    </div>

*/
    let videoContainer = document.querySelector('#home-video-container');
    let videoPlayer = videoContainer.querySelector('.home-video-player');
    let videoPlayerVideo = videoPlayer.querySelector('video');
    let videoPlayerVideoPlayOverlay = videoPlayer.querySelector('.play-overlay');

    let progress: HTMLElement = videoContainer.querySelector('.video-progress');
    let progressBar: HTMLElement = videoContainer.querySelector('.video-progress-in');

    let videoControls = videoContainer.querySelector('.video-controls');
    let playpause = videoControls.querySelector('.play-pause');

    videoPlayerVideo.controls = false;
    videoPlayerVideo.loop = true;
    videoPlayerVideo.preload = "auto";

    videoPlayerVideo.addEventListener('click', function(e) {
      if (videoPlayerVideo.paused || videoPlayerVideo.ended ){
        videoPlayerVideo.play();
      } else {
        videoPlayerVideo.pause();
      }
    });

    videoPlayerVideoPlayOverlay.addEventListener('click', function(e) {
      if (videoPlayerVideo.paused || videoPlayerVideo.ended ){
        if( !videoContainer.classList.contains('loading') ) {
          videoPlayerVideo.play();
        }
      } else {
        videoPlayerVideo.pause();
      }
    });

    // video state
    videoPlayerVideo.addEventListener('loadeddata', function(){
      console.log('loadeddata', videoPlayerVideo.readyState);
      if( videoPlayerVideo.readyState >= 2 ) {
        videoContainer.classList.remove('loading');
      }
    });

    // error
    videoPlayerVideo.addEventListener('error', function(e){
      console.log('error', this.error);
    });
    videoPlayerVideo.addEventListener('playing', function(e){
      videoContainer.classList.add('playing');
      videoContainer.classList.remove('paused');
    });
    videoPlayerVideo.addEventListener('pause', function(e){
      videoContainer.classList.add('paused');
      videoContainer.classList.remove('playing');
    });
    videoPlayerVideo.addEventListener('timeupdate', function(e) {
      progress.innerText = videoPlayerVideo.currentTime.toString();
      let progressBarDur = Math.floor((videoPlayerVideo.currentTime / videoPlayerVideo.duration) * 100);
      progressBar.style.width = progressBarDur + '%';
    });

    progress.addEventListener('click', function(e) {
      var t = this;
      // console.log(t);
      // var pos = (e.pageX - this.offsetLeft) / this.offsetWidth;
      // videoPlayerVideo.currentTime = pos * videoPlayerVideo.duration;
      var rect = t.getBoundingClientRect();
      // console.log(rect);
      var scrollLeft = window.pageXOffset || window.scrollX || document.documentElement.scrollLeft;
      // console.log(scrollLeft);
      var offsetLeft = rect.left + scrollLeft;
      var pos = (e.pageX - offsetLeft) / this.offsetWidth;
      var skippAhead = pos * videoPlayerVideo.duration;
      videoPlayerVideo.currentTime = skippAhead;
    });

    playpause.addEventListener('click', function(e) {
      // console.log('play/pause')
      if (videoPlayerVideo.paused || videoPlayerVideo.ended ){
        if( !videoContainer.classList.contains('loading') ) {
          videoPlayerVideo.play();
        }
      } else {
        videoPlayerVideo.pause();
      }
    });

    videoPlayerVideo.load();