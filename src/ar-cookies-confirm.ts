/*!
2017 (c) azre.xyz
version 2.0.0|20171112
MIT licence or Apache licence 2
- basically free for use
- leave note of copyright would be appreciated, thanks

Cookies confirm banner

*/

( (win:any): void => {

  "use strict";

  let doc: any = win.document;

  //let ar: any = win.ra || {};

  let cookiesConfirm = function(p?: string, d?: any){

    //help fce
    let isEmpty = (a: any) => {
      if( (typeof(a) === "undefined" || a == "" || a === null) ){
        return true;
      }
      return false;
    };
    let isObject = (a: any) => {
      if( typeof(a) === "object" ){
        return true;
      }
      return false;
    };
    let isUndefined = (a: any) => {
      if( typeof(a) === "undefined" ){
        return true;
      }
      return false;
    };

    //settings/option
    var set: any = {
      cookieName: 'cookie.policy.agreed',
      cookieVal: 'agree',
      expDays: 365,
      cssContainer: '',//frb
      cssInfo: '',
      cssBtn: 'btn btn-info',
      tranInfo: 'Our website use HTTP cookies for improve our services.',
      tranBtn: 'I understand'
    };

    //properties
    var property: any = {
      //main init
      init : function(opt?: any){
        //if settings/option esist
        if( ! isEmpty(opt) && typeof(opt) == "object" ){
          set = (<any>Object).assign({}, set, opt);
        }
        //console.log( set );
        var that = this;

        var html: any = {};
        html.body = $('body');

        /*doc.addEventListener("DOMContentLoaded", function () {
        });/**/

        if( ! that.getCookieExist( set.cookieName ) ){

          html.btn = $('<span></span>')
                .addClass('cp-btn ' + set.cssBtn)
                .html( set.tranBtn );
          html.info = $('<span></span>')
                .addClass('cp-info ' + set.cssInfo)
                .html( set.tranInfo );
          html.containerIn = $('<div></div>')
                .addClass('cp-in')
                .append( html.info, html.btn );
          html.container = $('<div></div>')
                .addClass('cookies-policy ' + set.cssContainer)
                .append( html.containerIn );
          html.body.prepend( html.container );

          html.container
              .on('click','.cp-btn', function(e: any){
                that.setCookie({n : set.cookieName, v : set.cookieVal});
                html.container.addClass('agreed');
              });

        }

      },//main init - END

      dayms: ( 24 * 60 * 60 * 1000 ),
      dto: function(){
        var dt = new Date();
        return new Date();
      },
      docCookies: function(d?: string){
        if( ! isEmpty(d) ){
          doc.cookie = d;
        }else{
          return doc.cookie;
        }
      },
      setCookie: function(d: any){
        if( ! isEmpty(d) && ! isEmpty(d.n) ){
          var dte = this.dto();
          dte.setTime( dte.getTime() + (set.expDays * this.dayms) );
          var exp = 'expires=' + dte.toUTCString();
          this.docCookies( d.n + '=' + d.v + '; ' + exp + '; path=/' );
        }else{
          return false;
        }
      },
      removeCookie: function(n: any){
        if( ! isEmpty(n) ){
          var exp = 'expires=Thu, 01 Jan 1970 00:00:00 UTC;';
          this.docCookies( n + '=; ' + exp + 'path=/' );
        }
      },
      getCookies: function(){
        var cc = this.docCookies().split(';');
        var ca = [];
        for( var i = cc.length - 1; i >= 0; i-- ){
          var c = cc[i];
          while(c.charAt(0) == ' '){ c = c.substring(1); }
          var cs = c.split('=');
          ca[ cs[0] ] = cs[1];
        }
        return ca;
      },
      getCookieByName: function(n: any){
        if( ! isEmpty(n) ){
          var v = this.getCookies()[n];
          return v;
        }
      },
      getCookieExist: function(n: any){
        if( ! isEmpty(n) ){
          var v = this.getCookies()[n];
          if( typeof( v ) != "undefined" ){
            return true;
          }else{
            return false;
          }
        }
      }

    };

    interface PropertyCall {
      [ key: string ]: any;
    }
    //call property
    if( isEmpty(p) || isObject(p) ){//empty init || init with object options
      return property['init'](p);
    }else if( ! isEmpty(p) && ! isUndefined((<PropertyCall>property)[p]) ){
      return property[p](d);
    }else{
      console.error('Error: Property not exist/undefined!');
    }

  };

  win.ar = win.ar || {};
  win.ar.cookiesConfirm = cookiesConfirm;

})( window );

