/*!
2022 (c) azre.xyz
version 0.1.0|20220107
MIT licence or Apache licence 2
- basically free for use
- leave note of copyright would be appreciated, thanks

Cookies operation:
  - documentCookies -> raw cookies/set cookie
  - set -> prepare cookie string to set cookie
  - remove -> remove cookie by name (set past date to cookie)
  - getAll -> get cookies in array
  - getCookieByName -> get specific cookie by name
  - isExist -> true/false - specific cookie exist

*/

interface CookiesSettings {
  path: string;
  domain: string;
}

((win: any): void => {
  'use strict';

  const doc: Document = win.document;

  // let ar: any = win.ra || {};

  const cookies = function (p: any, d: any): object {
    // help fce
    const isEmpty = (a: any): boolean => {
      if (typeof a === 'undefined' || a === '' || a === null || a === {}) {
        return true;
      }

      return false;
    };

    const isObject = (a: any): boolean => {
      if (typeof a === 'object') {
        return true;
      }

      return false;
    };

    const isEmptyObject = (a: object) => {
      return a && isObject(a) && Object.keys(a).length === 0 && a.constructor === Object;
    };

    const isUndefined = (a: any): boolean => {
      if (typeof a === 'undefined') {
        return true;
      }

      return false;
    };

    const getDate = (): Date => new Date();

    const stringIsObject = (a: string) => {
      try {
        JSON.parse(a);
        return true;
      } catch (error) {
        // console.error(error);
      }

      return false;
    };

    const parseCookieValue = (value: string) => {
      // object
      if (stringIsObject(value)) {
        return JSON.parse(value);
      }
      // number
      if (!isNaN(Number(value))) {
        return Number(value);
      }

      return value;
    };

    const dayInTimeVal: number = 24 * 60 * 60 * 1000; // day (24 h.) -> ms

    // settings/option
    let set: CookiesSettings = {
      path: '/',
      domain: '',
    };

    // interface ARcookiesProperties {
    //   init: object;
    //   getDate: object;
    //   documentCookies: object;
    //   set: object;
    //   remove: object;
    //   getAll: object;
    //   getCookieByName: object;
    //   isExist: object;
    // }
    // properties
    const property = {
      // main init
      init: function (opt?: object): void {
        // if settings/option esist
        if (!isEmpty(opt) && isObject(opt)) {
          set = (<any>Object).assign({}, set, opt);
        }
        // console.log( set );
        // console.log( this );
      }, // main init - END

      documentCookies: function (d?: any) {
        // console.log(d);
        if (!isEmpty(d)) {
          doc.cookie = d;
        } else {
          return doc.cookie;
        }
      },

      getAll: function (): { [key: string]: string } {
        const cc: string[] = this.documentCookies().split(';');

        const ca: { [key: string]: string } = {};
        cc.forEach((ci) => {
          const c = ci.trim();
          const cs: string[] = c.split('=');
          ca[cs[0]] = parseCookieValue(cs[1]);
        });

        return ca;
      },

      get: function (n: string): string | number | false {
        if (n && typeof n === 'string') {
          const v = this.getAll()[n] || null;
          return v;
        }

        return false;
      },

      isExist: function (n: string): boolean {
        return !!this.get(n);
      },

      set: function (data: {
        name: string;
        value: string | number | object;
        numberExpDays?: number;
        domain?: string;
        path?: string;
      }): boolean {
        if (!isEmpty(d) && !isEmpty(d.name)) {
          let c = '';
          c += data.name; // name

          // object value
          if (!isObject) {
            c += !isEmpty(data.value) ? '=' + data.value : ''; // value
          }

          // string or number or other
          if (isObject) {
            c += '=' + JSON.stringify(data.value); // value
          }

          // expiration
          if (!isEmpty(data.numberExpDays)) {
            const dte = getDate();
            dte.setTime(dte.getTime() + dayInTimeVal * data.numberExpDays);
            c += '; expires=' + dte.toUTCString();
          }

          c += '; path=' + set.path; // path
          // c += set.domain !== '' ? '; domain=' + set.domain : ''; // domain
          if (data.domain && !set.domain) {
            c += data.domain ? '; domain=' + data.domain : ''; // domain from data
          }
          if (!data.domain && set.domain) {
            c += set.domain ? '; domain=' + set.domain : ''; // domain from settings
          }

          this.documentCookies(c); // set cookie

          return true;
        }

        return false;
      },

      remove: function (n: string): boolean {
        if (!isEmpty(n)) {
          const expiration = 'expires=Thu, 01 Jan 1970 00:00:00 UTC;';
          this.documentCookies(n + '=; ' + expiration + 'path=/');

          return true;
        }

        return false;
      },
    };

    interface PropertyCall {
      [key: string]: any;
    }

    // call property
    if (isEmpty(p) || isObject(p)) {
      // empty init || init with object options
      return (<PropertyCall>property).init(p);
    } else if (!isEmpty(p) && !isUndefined((<PropertyCall>property)[p])) {
      return (<PropertyCall>property)[p](d);
    } else {
      console.log('ERROR: arCookies property "' + p + '" not exist!');
    }
  };

  win.ar = win.ar || {};
  win.ar.cookies = cookies;
})(window);
