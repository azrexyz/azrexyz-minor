/*!
2018 (c) azre.xyz
version 0.1.0|20180127
MIT licence or Apache licence 2
- basically free for use
- leave note of copyright would be appreciated, thanks

Bootstrap (v4.x) modal - controller
object call by: ar.bsModal
modal elements:
  - modal
    - modalDialog
      - modalContent
        - modalHeader
          - modalTitle
          - modalClose
        - modalBody
        - modalFooter

data modal parts: title, body, footer

modal methods:
  - init - initialization (triggered onload after bsModal obejct creation)
  - isInitialized - true/false bsModal.modal item is set (not undefined)
  - setTitle(d: any) -> set content/html for modal title || empty hide header (title) part
  - setBody(d: any) -> set content/html for modal body
  - setFooter(d: any) -> set content/html for modal footer || empty hide footer part
  - update(d: {title?: any, body?: any, footer?: any}) -> set all parts by set functions above
  - show(d?: {title?: any, body?: any, footer?: any}) -> show mdal and optionaly modalData
  - hide() - hide modal and erase data from all parts

-------------------------------------------------------------------------------
example:
var modalExampleData = {
    title: 'Modal title',
    body: 'Modal body content',
    footer: 'Modal footer content',
};
//ar.bsModal.update( modalExampleData );
ar.bsModal.show( modalExampleData );
-------------------------------------------------------------------------------
*/

declare var $: any;

(function(win: any): void{
    "use strict";

    let doc: any = win.document;
    let body: any = doc.body;

    win.ar = win.ar || {};

    interface bsModalData{
        title?: any,
        body?: any,
        footer?: any,
    }

    //create div element with CSS class
    let cDiv = function(s: string){
        return $('<div></div>').addClass(s);
    };

    let bsModal: any = {
        // variables
        modal: cDiv('modal'),
        modalDialog: cDiv('modal-dialog'),
        modalHeader: cDiv('modal-header'),
        modalContent: cDiv('modal-content'),
        modalTitle: cDiv('modal-title'),
        modalClose: cDiv('close'),
        modalBody: cDiv('modal-body'),
        modalFooter: cDiv('modal-footer'),

        // properties
        init: function(){
            this.modalHeader.append( this.modalTitle );
            this.modalContent.append(
                this.modalHeader,
                this.modalBody,
                this.modalFooter
            );
            this.modalDialog.append( this.modalContent );
            this.modal.append( this.modalDialog );

            //append modal to body
            $(body).append( bsModal.modal );
        },

        //check if modal exists
        isInitialized: function(){
            if( this.modal ){
                return true;
            }
            return false;
        },

        //set/change modal title
        setTitle: function(d: any){
            if( d ){
                this.modalHeader.removeClass('d-none');
                this.modalTitle.html(d);
            }else{
                this.modalHeader.addClass('d-none');
                this.modalTitle.html('');
            }
        },

        //set/change modal body
        setBody: function(d: any){
            this.modalBody.html(d);
        },

        //set/change modal footer
        setFooter: function(d: any){
            if( d ){
                this.modalFooter
                    .removeClass('d-none')
                    .html(d);
            }else{
                this.modalFooter
                    .addClass('d-none')
                    .html('');
            }
        },

        //set/change modal data parts => title, body, footer
        update: function(d: bsModalData){
            if( typeof(d.title) !== "undefined" ){
                this.setTitle( d.title );
            }
            if( typeof(d.body) !== "undefined" ){
                this.setBody( d.body );
            }
            if( typeof(d.footer) !== "undefined" ){
                this.setFooter( d.footer );
            }
        },

        //show modal
        show: function( d: bsModalData ){
            if( d ){
                this.update(d);
            }
            this.modal.modal('show');
        },

        //hide modal
        hide: function(){
            let that = this;
            that.modal.modal('hide');
            setTimeout(function(){
                that.modalTitle.html('');
                that.modalBody.html('');
                that.modalFooter.html('');
            }, 1000);
        },

    };

    bsModal.init();

    win.ar.bsModal = bsModal;

})( window );