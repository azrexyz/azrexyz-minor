/*!
2016 (c) azre.xyz
version 1.12
MIT licence or Apache licence 2
- basically free for use
- leave note of copyright would be appreciated, thanks

options:

*/
(function ($) {
    $.fn.emailAntispam = function (opt) {
        var set = {
            'emailAT': '[at]',
            'emailDOT': '[dot]',
            'eparser': ',',
            'enam': 'info',
            'edom': 'domain',
            'etld': 'cz'
        };
        if (opt) {
            $.extend(set, opt);
        }
        this.each(function () {
            var e = $(this);
            var p = e.html().split(set.eparser);
            if (typeof (p[0]) === "undefined" || p[0] == "") {
                p[0] = set.enam;
            }
            if (typeof (p[1]) === "undefined" || p[1] == "") {
                p[1] = set.edom;
            }
            if (typeof (p[2]) === "undefined" || p[2] == "") {
                p[2] = set.etld;
            }
            e.attr('href', 'mailto:' + p[0] + '@' + p[1] + '.' + p[2]);
            if (typeof (p[3]) === "undefined") {
                e.html(p[0] + set.emailAT + p[1] + set.emailDOT + p[2]);
            }
            else {
                e.html(p[3]);
            }
        });
    };
})(jQuery);
//# sourceMappingURL=ar-jq-antispamemail.js.map