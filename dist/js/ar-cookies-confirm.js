/*!
2017 (c) azre.xyz
version 2.0.0|20171112
MIT licence or Apache licence 2
- basically free for use
- leave note of copyright would be appreciated, thanks

Cookies confirm banner

*/
(function (win) {
    "use strict";
    var doc = win.document;
    var cookiesConfirm = function (p, d) {
        var isEmpty = function (a) {
            if ((typeof (a) === "undefined" || a == "" || a === null)) {
                return true;
            }
            return false;
        };
        var isObject = function (a) {
            if (typeof (a) === "object") {
                return true;
            }
            return false;
        };
        var isUndefined = function (a) {
            if (typeof (a) === "undefined") {
                return true;
            }
            return false;
        };
        var set = {
            cookieName: 'cookie.policy.agreed',
            cookieVal: 'agree',
            expDays: 365,
            cssContainer: '',
            cssInfo: '',
            cssBtn: 'btn btn-info',
            tranInfo: 'Our website use HTTP cookies for improve our services.',
            tranBtn: 'I understand'
        };
        var property = {
            init: function (opt) {
                if (!isEmpty(opt) && typeof (opt) == "object") {
                    set = Object.assign({}, set, opt);
                }
                var that = this;
                var html = {};
                html.body = $('body');
                if (!that.getCookieExist(set.cookieName)) {
                    html.btn = $('<span></span>')
                        .addClass('cp-btn ' + set.cssBtn)
                        .html(set.tranBtn);
                    html.info = $('<span></span>')
                        .addClass('cp-info ' + set.cssInfo)
                        .html(set.tranInfo);
                    html.containerIn = $('<div></div>')
                        .addClass('cp-in')
                        .append(html.info, html.btn);
                    html.container = $('<div></div>')
                        .addClass('cookies-policy ' + set.cssContainer)
                        .append(html.containerIn);
                    html.body.prepend(html.container);
                    html.container
                        .on('click', '.cp-btn', function (e) {
                        that.setCookie({ n: set.cookieName, v: set.cookieVal });
                        html.container.addClass('agreed');
                    });
                }
            },
            dayms: (24 * 60 * 60 * 1000),
            dto: function () {
                var dt = new Date();
                return new Date();
            },
            docCookies: function (d) {
                if (!isEmpty(d)) {
                    doc.cookie = d;
                }
                else {
                    return doc.cookie;
                }
            },
            setCookie: function (d) {
                if (!isEmpty(d) && !isEmpty(d.n)) {
                    var dte = this.dto();
                    dte.setTime(dte.getTime() + (set.expDays * this.dayms));
                    var exp = 'expires=' + dte.toUTCString();
                    this.docCookies(d.n + '=' + d.v + '; ' + exp + '; path=/');
                }
                else {
                    return false;
                }
            },
            removeCookie: function (n) {
                if (!isEmpty(n)) {
                    var exp = 'expires=Thu, 01 Jan 1970 00:00:00 UTC;';
                    this.docCookies(n + '=; ' + exp + 'path=/');
                }
            },
            getCookies: function () {
                var cc = this.docCookies().split(';');
                var ca = [];
                for (var i = cc.length - 1; i >= 0; i--) {
                    var c = cc[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    var cs = c.split('=');
                    ca[cs[0]] = cs[1];
                }
                return ca;
            },
            getCookieByName: function (n) {
                if (!isEmpty(n)) {
                    var v = this.getCookies()[n];
                    return v;
                }
            },
            getCookieExist: function (n) {
                if (!isEmpty(n)) {
                    var v = this.getCookies()[n];
                    if (typeof (v) != "undefined") {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        };
        if (isEmpty(p) || isObject(p)) {
            return property['init'](p);
        }
        else if (!isEmpty(p) && !isUndefined(property[p])) {
            return property[p](d);
        }
        else {
            console.error('Error: Property not exist/undefined!');
        }
    };
    win.ar = win.ar || {};
    win.ar.cookiesConfirm = cookiesConfirm;
})(window);
//# sourceMappingURL=ar-cookies-confirm.js.map