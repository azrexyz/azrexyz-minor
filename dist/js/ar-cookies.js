/*!
2017 (c) azre.xyz
version 0.0.5|20171115
MIT licence or Apache licence 2
- basically free for use
- leave note of copyright would be appreciated, thanks

Cookies operation:
  - docCookies -> raw cookies/set cookie
  - setCookie -> prepare cookie string to set cookie
  - removeCookie -> remove cookie by name (set past date to cookie)
  - getCookies -> get cookies in array
  - getCookieByName -> get specific cookie by name
  - getCookieExist -> true/false - specific cookie exist

*/
(function (win) {
    "use strict";
    var doc = win.document;
    var cookies = function (p, d) {
        var isEmpty = function (a) {
            if ((typeof (a) === "undefined" || a == "" || a === null)) {
                return true;
            }
            return false;
        };
        var isObject = function (a) {
            if (typeof (a) === "object") {
                return true;
            }
            return false;
        };
        var isUndefined = function (a) {
            if (typeof (a) === "undefined") {
                return true;
            }
            return false;
        };
        var set = {
            tms: (24 * 60 * 60 * 1000),
            path: '/',
            domain: ''
        };
        var property = {
            init: function (opt) {
                if (!isEmpty(opt) && isObject(opt)) {
                    set = Object.assign({}, set, opt);
                }
            },
            dto: function () {
                return new Date();
            },
            docCookies: function (d) {
                if (!isEmpty(d)) {
                    doc.cookie = d;
                }
                else {
                    return doc.cookie;
                }
            },
            setCookie: function (d) {
                if (!isEmpty(d) && !isEmpty(d.n)) {
                    var c = '';
                    c += d.n;
                    c += (!isEmpty(d.v) ? '=' + d.v : '');
                    if (!isEmpty(d.exp)) {
                        var dte = this.dto();
                        dte.setTime(dte.getTime() + (set.tms * d.exp));
                        c += '; expires=' + dte.toUTCString();
                    }
                    c += '; path=' + set.path;
                    c += (set.domain != '' ? '; domain=' + set.domain : '');
                    this.docCookies(c);
                    return true;
                }
                else {
                    return false;
                }
            },
            removeCookie: function (n) {
                if (!isEmpty(n)) {
                    var exp = 'expires=Thu, 01 Jan 1970 00:00:00 UTC;';
                    this.docCookies(n + '=; ' + exp + 'path=/');
                }
            },
            getCookies: function () {
                var cc = this.docCookies().split(';');
                var ca = [];
                var cs = [];
                for (var i = cc.length - 1; i >= 0; i--) {
                    var c = cc[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    var cs_1 = c.split('=');
                    ca[cs_1[0]] = cs_1[1];
                }
                return ca;
            },
            getCookieByName: function (n) {
                if (!isEmpty(n)) {
                    var v = this.getCookies()[n];
                    return v;
                }
            },
            getCookieExist: function (n) {
                if (!isEmpty(n)) {
                    var v = this.getCookies()[n];
                    if (typeof (v) !== "undefined") {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        };
        if (isEmpty(p) || isObject(p)) {
            return property['init'](p);
        }
        else if (!isEmpty(p) && !isUndefined(property[p])) {
            return property[p](d);
        }
        else {
            console.log('ERROR: arCookies property \"' + p + '\" not exist!');
        }
    };
    win.ar = win.ar || {};
    win.ar.cookies = cookies;
})(window);
//# sourceMappingURL=ar-cookies.js.map