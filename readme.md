# AzRe minor

- usesfull libraries/framework/utilities/code
- more will be soon on our website https://www.azre.xyz/

## AR Cookies

- library for control cookies

## AR Bootstrap modal control

- library for control Bootstrap modal
- one dialog with rewitable content
- most used operations with modal and simplifying modal access

## Checkbox toggle (only CSS/SASS)

- design html input checkbox `<input type="checkbox">` with some html elements
- code only CSS based (no JS)
- customizable CSS (SASS)
